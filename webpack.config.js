const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
    entry: './src/main/resources/static/vue/main.js',
    output: {
        path: path.resolve(__dirname, 'target/classes/static/js'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};